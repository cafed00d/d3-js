function get_radii_for_cities(cities_var) {

var radius = d3.scale.sqrt()
     .domain([0, 1e7])
     .range([0, 200]);

var radii = Object();
            for (i = 0; i < cities_var.length; i += 1) {
                var lat = cities_var[i].geometry.coordinates[1];
                var lon = cities_var[i].geometry.coordinates[0];
                var _url = "http://localhost:8983/solr/collection1/select?q=*:*&fq={!geofilt%20sfield=LatLong}&pt=" + lat + "," + lon + "&d=15" + "&facet=true&facet.field=LatLong&bf=recip(geodist(),2,200,20)&sort=score%20desc&rows=0&wt=json"
                $.ajax({
                    url: _url,
                    context: document.body,
                    dataType: 'json',
                    async: false,
                    success: function(R) {
                        var key = cities_var[i].geometry.coordinates[0] + " " + cities_var[i].geometry.coordinates[1];
                        radii[key] = radius(R.response.numFound);                        
                        }
                });
            }


return radii;
}
