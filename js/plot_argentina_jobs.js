function make_ajax_and_plot(_url, g) {
var json = Array();
    $.ajax({
        url: _url,
        context: document.body,
        dataType: 'json',
        async: false,
        success: function(R) {
        
        
        for (i =0 ;i < R.response.docs.length; i += 1) {
            var point = Object();
            var coords = Array();
            
            
            var LatLong = R.response.docs[i].LatLong; 
            coords[0] = parseFloat(LatLong.split(",")[1]);
            coords[1] = parseFloat(LatLong.split(",")[0]);

            point["type"] = "Point";
            point["coordinates"] = coords;
            json.push(point);
        }
        },
        
        error: function(er) {
            console.log("error " + er);
        }
    });
        
    g.append("g")
        .attr("class", "cities")
            .selectAll("circle")
            .data(json)//[{"type": "Point", "coordinates": [-59.0395970599041,-34.090187877633554]}])
            .enter().append("circle")
            .attr("transform", function(d) {
                var ctr = Object();
                ctr["type"] = "Feature";
                ctr["geometry"] = d;
                return "translate(" + path.centroid(ctr) + ")";
            })
            .attr("class", "argentina")
            .attr("r", function(city) {
             return 1.5;
    });
    
}

function plot_argentina_jobs(g) {

    var solr_url = "http://localhost:8983/solr/collection1/";
    var argentina_jobs_url = "select?q=*:*&fq=LatLong:[-27.426955,-68.116809%20TO%20-24.723463,-59.877064]&fl=LatLong&rows=90000";
    var response_type = "&wt=json";
    
    var _url = solr_url + argentina_jobs_url + response_type;
    console.log(_url);
    var json = make_ajax_and_plot(_url, g);
    
    var _urlbox2 = "http://10.0.4.30:8983/solr/collection1/select?q=*:*&fq=LatLong:[-35.224604,-70.050403%20TO%20-28.204298,-56.734974]&fl=LatLong&rows=90000&wt=json";
    make_ajax_and_plot(_urlbox2, g);
    
    var _urlbox3 = "http://localhost:8983/solr/collection1/select?q=*:*&fq=LatLong:[-54.403444,-72.116911%20TO%20-34.776164,-56.981871]&fl=LatLong&rows=90000&wt=json";
    make_ajax_and_plot(_urlbox3, g);


}
