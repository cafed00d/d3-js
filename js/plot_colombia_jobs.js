function plot_colombia_jobs(g) {

    var solr_url = "http://localhost:8983/solr/collection1/";
    var cambodia_jobs_url = "select?q=*:*&fq=LatLong:[-2.549022,-77.571456%20TO%2013.666332,-72.298019]&fl=LatLong&rows=90000";
    var response_type = "&wt=json";
    
    var _url = solr_url + cambodia_jobs_url + response_type;
    console.log(_url);
    var json = Array();
    $.ajax({
        url: _url,
        context: document.body,
        dataType: 'json',
        async: false,
        success: function(R) {
        
        
        for (i =0 ;i < R.response.docs.length; i += 1) {
            var point = Object();
            var coords = Array();
            
            
            var LatLong = R.response.docs[i].LatLong; 
            coords[0] = parseFloat(LatLong.split(",")[1]);
            coords[1] = parseFloat(LatLong.split(",")[0]);

            point["type"] = "Point";
            point["coordinates"] = coords;
            json.push(point);
        }
        },
        
        error: function(er) {
            console.log("error " + er);
        }
    });
    
    
    console.log(json);
    
    var ctr = {"type": "Point", "coordinates": [-59.0395970599041,-34.090187877633554]};
    g.append("g")
        .attr("class", "cities")
            .selectAll("circle")
            .data(json)//[{"type": "Point", "coordinates": [-59.0395970599041,-34.090187877633554]}])
            .enter().append("circle")
            .attr("transform", function(d) {
                var ctr = Object();
                ctr["type"] = "Feature";
                ctr["geometry"] = d;
                return "translate(" + path.centroid(ctr) + ")";
            })
            .attr("class", "colombia")
            .attr("r", function(city) {
             return 1.0;
    });

}
